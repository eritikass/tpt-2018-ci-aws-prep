# install docker-compose 

 * https://docs.docker.com/compose/install/

# create network for traefik

```bash
docker network create traefik
```

# run traefik using docker-compose

```bash
docker-compose up -d
# show logs 

docker-compose logs

# if you want to restart
docker-compose down -v && docker-compose up -d
```

# test run container

```bash
docker run --name=test1 --rm -it --network traefik -P -d -l "traefik.enable=true" -l 'traefik.http.routers.test1.rule=Host(`test1.EXAMPLE.test125.eu`)' -l "traefik.http.routers.test1.entrypoints=websecure" -l "traefik.http.routers.test1.tls.certresolver=myresolver" nginx
```

